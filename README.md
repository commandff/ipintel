# ipintel
-- import "gitlab.com/commandff/ipintel"

The ipiptel package provides a caching go interface to the http://getipintel.net/ api using the non-json interface.
This package is working, but poorly documented. Todo.

## Usage

## Variables
```go
var (
	// Maximum Cache age to return
	MaxCacheAge time.Duration = 48 * time.Hour

	// Maximum time to wait for rate limit
	MaxWaitTime time.Duration = 5 * time.Second

	// optional- redirect logs elsewhere.
	Logger *log.Logger = log.Default()

	// "flags" input to api.
	// ex: ipintel.CheckType = ipintel.CheckTypeDynamic
	//     ipintel.CheckType = ipintel.CheckTypeStatic + ipintel.CheckTypeOptN
	CheckType string = CheckTypeDynamic

	// "oflags" input to api.
	// ex: ipintel.CheckOpt = ipintel.CheckOptNil
	//     ipintel.CheckOpt = ipintel.CheckOptBadIP
	//     ipintel.CheckOpt = ipintel.CheckOptBadIP + ipintel.CheckOptCountry
	CheckOpt string = ""

	// API Parts
	APISchema   string = "http"
	APIDomain   string = "check.getipintel.net"
	APIEndpoint string = "/check.php"

	// set this!
	APIContact string

	// Whitelisted IP Blocks
	Whitelist []string = []string{
		"127.0.0.0/8",    // IPv4 loopback
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"169.254.0.0/16", // RFC3927 link-local
	}
)
```
Some of these should be set to things.

## Example
```go
package main

import (
	"fmt"

	"gitlab.com/commandff/ipintel"
)

func main() {
	ipintel.CheckType = ipintel.CheckTypeStatic
	ipintel.CheckOpt = ipintel.CheckOptBadIP + ipintel.CheckOptCountry + ipintel.CheckOptiCloud
	ipintel.APIContact = "commandff@gmail.com"
	ipintel.Init()

	fmt.Printf("icloud ip:\n%#v\n\n", ipintel.GetResult("172.224.224.34"))
	fmt.Printf("normal ip:\n%#v\n\n", ipintel.GetResult("47.40.176.253"))
}

```

## Minimum Example
```go
package main

import (
	"fmt"

	"gitlab.com/commandff/ipintel"
)

func main() {
	ipintel.APIContact = "commandff@gmail.com"
	ipintel.Init()

	fmt.Printf("icloud ip:\n%#v\n\n", ipintel.GetResult("172.224.224.34"))
}

```
## Result
every call go GetResult will return a pointer to the result struct.
if `lastChecked` == `time.Date(1, time.January, 1, 0, 0, 0, 0, time.UTC)`, there was an error of some sort, and no check was completed.


```go
type Result struct {
	ProxyChance float64
	IsBad       bool
	IsiCloud    bool
	Country     string
	LastChecked time.Time
	LastSeen    time.Time
}
```

#### func  Cleanup

```go
func Cleanup()
```
Cleans up expired entries- should be called every couple days.


#### func  Init

```go
func Init()
```
Should be called before GetResult, to parse the default whitelist, and validate contact email


#### func  Parse_whitelist

```go
func Parse_whitelist()
```
If you update the whitelist externally, you can call this to update the internal whitelist.


#### func  GetResult

```go
func GetResult(ip string) *Result
```
Get Result - either from cache, or fresh


#### func  BuildReqURL

```go
func BuildReqURL(ip string) string
```
Assembles request url


#### func  IsWhiteListIP

```go
func IsWhiteListIP(ip net.IP) bool
```
checks if the given ip is in the whitelist


#### func  SanitizeEmail

```go
func SanitizeEmail(mightBeEmail string) (email string)
```
validates that email address conforms to standards, and has a valid domain, with a mail exchanger.
