package ipintel

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/mail"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/juju/ratelimit"
)

type Result struct {
	ProxyChance float64   `json:"proxy_chance"`
	IsBad       bool      `json:"is_bad"`
	IsiCloud    bool      `json:"is_iCloud"`
	Country     string    `json:"country"`
	LastChecked time.Time `json:"last_checked"`
	LastSeen    time.Time `json:"last_seen"`
}

const (
	CheckTypeStatic  = "m" // flags=m is used when you're only looking for the value of "1" as the result. The m flag skips the dynamic checks and only uses dynamic ban lists. See Variations of Implementation and What are dynamic checks? for detailed explanation.
	CheckTypeDynamic = "b" // flags=b is used when you want to use dynamic ban and dynamic checks with partial bad IP check. See Variations of Implementation for detailed explanation.
	CheckTypeFull    = "f" // flags=f is used when you want to force the system to do a full lookup, which can take up to 5 seconds. See Variations of Implementation for detailed explanation.

	CheckTypeOptN = "n" // flags=n is used to exclude real time block list. Append the character "n" if you're already using flags=m, b, or f. For example, flags=nm.

	CheckOptNil     = ""  // omit the oflags option
	CheckOptBadIP   = "b" // oflags=b is used when you want to see if the IP is considered as bad IP. Note that when using flags option, this result can vary due to the included datasets. Please see the comparsion table for more information.
	CheckOptCountry = "c" // oflags=c is used when you want to see which country the IP came from / which country the IP belongs to (GeoIP Location). Currently in alpha testing.
	CheckOptiCloud  = "i" // oflags=i is used when you want to exclude iCloud Relay Egress IPs. They are by definition a proxy/VPN IP, however, having this additional data may help you make a more informed decision.
	UserAgent       = "ipintel.go/v1 <commandff@gmail.com>"
)

var (
	// Maximum Cache age to return
	MaxCacheAge time.Duration = 48 * time.Hour

	// Maximum time to wait for rate limit
	MaxWaitTime time.Duration = 5 * time.Second

	// optional- redirect logs elsewhere.
	Logger *log.Logger = log.Default()

	// "flags" input to api.
	// ex: ipintel.CheckType = ipintel.CheckTypeDynamic
	//     ipintel.CheckType = ipintel.CheckTypeStatic + ipintel.CheckTypeOptN
	CheckType string = CheckTypeDynamic

	// "oflags" input to api.
	// ex: ipintel.CheckOpt = ipintel.CheckOptNil
	//     ipintel.CheckOpt = ipintel.CheckOptBadIP
	//     ipintel.CheckOpt = ipintel.CheckOptBadIP + ipintel.CheckOptCountry
	CheckOpt string = ""

	// API Parts
	APISchema   string = "http"
	APIDomain   string = "check.getipintel.net"
	APIEndpoint string = "/check.php"

	// set this!
	APIContact string

	// Whitelisted IP Blocks
	Whitelist []string = []string{
		"127.0.0.0/8",    // IPv4 loopback
		"10.0.0.0/8",     // RFC1918
		"172.16.0.0/12",  // RFC1918
		"192.168.0.0/16", // RFC1918
		"169.254.0.0/16", // RFC3927 link-local
	}
)

var (
	whitelist   []*net.IPNet
	cacheMutex  sync.RWMutex
	rateLimiter *ratelimit.Bucket  = ratelimit.NewBucketWithQuantum(4*time.Second, 15, 1)
	httpClient  http.Client        = http.Client{Timeout: 10 * time.Second}
	cache       map[string]*Result = make(map[string]*Result)
)

// Cleans up expired entries- should be called every couple days.
func Cleanup() {
	cacheMutex.Lock()
	for ip, v := range cache {
		if time.Since(v.LastSeen) > MaxCacheAge {
			delete(cache, ip)
		}
	}
	cacheMutex.Unlock()
}

func SaveCache(fn string) int {
	jsonString, err := json.Marshal(&cache)
	if err != nil {
		Logger.Printf("IPIntel ERROR %v", err)
		return -1
	}
	ioutil.WriteFile(fn, jsonString, 0655)
	Logger.Printf("IPIntel Saved %d entries to %s", len(cache), fn)
	return len(cache)
}

func LoadCache(fn string) int {
	content, err := ioutil.ReadFile(fn)
	if err != nil {
		Logger.Printf("IPIntel ERROR %v", err)
		return -1
	}
	err = json.Unmarshal(content, &cache)
	if err != nil {
		Logger.Printf("IPIntel ERROR %v", err)
		return -1
	}
	Logger.Printf("IPIntel Loaded %d entries from %s", len(cache), fn)
	return len(cache)
}

// Should be called before GetResult, to parse the default whitelist, and validate contact email
func Init() {
	Parse_whitelist()
	if len(SanitizeEmail(APIContact)) == 0 {
		Logger.Printf("IPIntel ERROR: Invalid Email address!")
		return
	}
}

// If you update the whitelist externally, you can call this to update the internal whitelist.
func Parse_whitelist() {
	for _, cidr := range Whitelist {
		_, block, err := net.ParseCIDR(cidr)
		if err != nil {
			panic(fmt.Errorf("parse error on %q: %v", cidr, err))
		}
		whitelist = append(whitelist, block)
	}
}

// Get Result - either from cache, or fresh
func GetResult(ip string) *Result {
	cacheMutex.RLock()
	ret, ok := cache[ip]
	if ok && time.Until(ret.LastChecked.Add(MaxCacheAge)) > 0 { // blame
		ret.LastSeen = time.Now()
		cacheMutex.RUnlock()
		return ret
	}
	cacheMutex.RUnlock()
	return getResult(ip)
}

// Assembles request url
func BuildReqURL(ip string) string {
	if len(CheckOpt) > 0 {
		return fmt.Sprintf(
			"%s://%s/%s?ip=%s&contact=%s&flags=%s&oflags=%s",
			APISchema, APIDomain, APIEndpoint,
			ip, APIContact, CheckType, CheckOpt)
	}
	return fmt.Sprintf(
		"%s://%s/%s?ip=%s&contact=%s&flags=%s",
		APISchema, APIDomain, APIEndpoint,
		ip, APIContact, CheckType)
}

// checks if the given ip is in the whitelist
func IsWhiteListIP(ip net.IP) bool {
	if ip.IsLoopback() || ip.IsLinkLocalUnicast() || ip.IsLinkLocalMulticast() {
		return true
	}
	for _, block := range whitelist {
		if block.Contains(ip) {
			return true
		}
	}
	return false
}

// validates that email address conforms to standards, and has a valid domain, with a mail exchanger.
func SanitizeEmail(mightBeEmail string) (email string) {
	if len(mightBeEmail) < 3 || len(mightBeEmail) > 254 {
		return ""
	}
	add, err := mail.ParseAddress(mightBeEmail)
	if err != nil {
		return ""
	}
	parts := strings.Split(add.Address, "@")
	mx, err := net.LookupMX(parts[1])
	if err != nil || len(mx) < 1 {
		return ""
	} else {
		return add.Address
	}
}

// Internal getResult
func getResult(ip string) (result *Result) {
	result = &Result{
		LastSeen: time.Now(),
	}
	cacheMutex.Lock()
	cache[ip] = result
	cacheMutex.Unlock()

	if IsWhiteListIP(net.ParseIP(ip)) {
		return
	}
	if ok := rateLimiter.WaitMaxDuration(1, MaxWaitTime); !ok {
		return
	}
	url := BuildReqURL(ip)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		Logger.Printf("IPIntel Parse %s failed: %v", url, err)
		return
	}
	req.Header.Add("User-Agent", UserAgent)
	resp, err := httpClient.Do(req)
	if err != nil {
		Logger.Printf("IPIntel Request %s failed: %v", url, err)
		return
	}
	defer resp.Body.Close()
	b, err := io.ReadAll(resp.Body)
	if err != nil {
		Logger.Printf("IPIntel failed to read response body")
		return
	}
	data := strings.Split(strings.Trim(string(b), "\"'\r\n\x00"), ",")
	if len(data) > 0 && resp.StatusCode == 200 {

		result.LastChecked = time.Now()
		consumed := 0
		result.ProxyChance, err = strconv.ParseFloat(data[consumed], 32)
		if err != nil {
			Logger.Printf("IPIntel Unable to parse response: \"%s\"", data[consumed])
			return
		}
		consumed++

		if len(data) > consumed && strings.Contains(CheckOpt, CheckOptBadIP) {
			result.IsBad, err = strconv.ParseBool(data[consumed])
			if err != nil {
				Logger.Printf("IPIntel Unable to parse response: \"%s\"", data[consumed])
				return
			}
			consumed++
		}
		if len(data) > consumed && strings.Contains(CheckOpt, CheckOptiCloud) {
			result.IsiCloud, err = strconv.ParseBool(data[consumed])
			if err != nil {
				Logger.Printf("IPIntel Unable to parse response: \"%s\"", data[consumed])
				return
			}
			consumed++
		}
		if len(data) > consumed && strings.Contains(CheckOpt, CheckOptCountry) {
			result.Country = data[consumed]
			//consumed++
		}
		return
	} else if resp.StatusCode == 400 {
		data := strings.Trim(string(b), "\"'\r\n\x00")
		errCode, err := strconv.ParseInt(data, 10, 32)
		if err != nil {
			Logger.Printf("IPIntel Unable to parse error response: %s  %#v", string(b), b)
			return
		}
		switch errCode {
		case -1:
			Logger.Printf("IPIntel ERROR: Invalid no input")
		case -2:
			Logger.Printf("IPIntel ERROR: Invalid IP address")
		case -3:
			Logger.Printf("IPIntel ERROR: Unroutable address / private address")
		case -4:
			Logger.Printf("IPIntel ERROR: Unable to reach database, most likely the database is being updated. Keep an eye on twitter for more information.")
		case -5:
			Logger.Printf("IPIntel ERROR: Your connecting IP has been banned from the system or you do not have permission to access a particular service. Did you exceed your query limits? Did you use an invalid email address?")
		case -6:
			Logger.Printf("IPIntel ERROR: You did not provide any contact information with your query or the contact information is invalid.")
		}
		return
	} else if resp.StatusCode == 429 {
		Logger.Printf("IPIntel ERROR: Throttled")
		return
	}
	return
}
